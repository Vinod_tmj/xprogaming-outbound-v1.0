package com.river.gateway.service.outbound.xprogaming.v1.handlers;

import java.io.StringReader;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.perf4j.LoggingStopWatch;
import org.perf4j.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import com.river.gateway.service.beans.UserData;
import com.river.gateway.service.cache.UserDataCache;
import com.river.gateway.service.integration.fund.object.ECRInfo;
import com.river.gateway.service.listener.object.UserLoginData;
import com.river.gateway.service.message.handlers.IMessageHandler;
import com.river.gateway.service.messages.common.IMessage;
import com.river.gateway.service.messages.outbound.OutboundUserRequest;
import com.river.gateway.service.messages.outbound.OutboundUserResponse;
import com.river.gateway.service.outbound.xprogaming.v1.XProGamingOutboundAdaptor;
import com.river.gateway.service.outbound.xprogaming.v1.logger.XProGamingOutboundLogger;
import com.river.gateway.service.outbound.xprogaming.v1.messages.GameRequest;
import com.river.gateway.service.outbound.xprogaming.v1.messages.Response;
import com.river.gateway.service.outbound.xprogaming.v1.utils.XProGamingUtils;
import com.river.gateway.service.utils.VendorConstants;
import com.river.gateway.service.utils.VendorConstants.VendorResponseParamKey;
import com.river.gateway.service.utils.VendorUtils;

public class GameRequestHandler extends AbstractVendorHandler implements IMessageHandler {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public RedisTemplate<String, Object> getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public IMessage processOutboundMessage(IMessage message, String correlationId, String replyTo) {
        if (message instanceof OutboundUserRequest) {
            OutboundUserRequest outboundUserRequest = (OutboundUserRequest) message;

            return handleMessage(outboundUserRequest, correlationId, replyTo);
        } else {
            XProGamingOutboundLogger.error("Unknow message {} " + message);
        }
        return null;
    }

    private IMessage handleMessage(OutboundUserRequest outboundUserRequest, String correlationId, String replyTo) {
        StopWatch stopWatch = new LoggingStopWatch("openGame-GM-handleMessage");
        /*
         * Handle all the race conditions happening from other requests.
         */
        XProGamingOutboundLogger.info("Getting user data from cache");
        UserData userData = UserDataCache.getUserDataForEcrVendorId(outboundUserRequest.getEcrExternalId(),
                XProGamingOutboundAdaptor.vendorId.toString());
        XProGamingOutboundLogger.info("Retrieved user data from cache = " + userData);

        if (userData == null) {

            userData = new UserData();
            userData.setEcrId(outboundUserRequest.getEcrId());
            userData.setEcrExternalId(outboundUserRequest.getEcrExternalId());
            userData.setVendorId(XProGamingOutboundAdaptor.vendorId.toString());
            userData.setEcrVendorId(outboundUserRequest.getEcrExternalId());
            UserLoginData userLoginData = new UserLoginData();

            userLoginData.setEcrCurrency(userData.getEcrCurrency());
            userLoginData.setEcrExternalID(userData.getEcrExternalId());
            userLoginData.setEcrID(userData.getEcrId());
            userLoginData.setLabelID(outboundUserRequest.getLabelId());
            userLoginData.setPartnerID(outboundUserRequest.getPartnerId());
            userLoginData.setProductID(outboundUserRequest.getProductId());

            userData.setUserLoginData(userLoginData);

            boolean success = vendorService.updateUserData(userData);

            if (!success) {
                XProGamingOutboundLogger.warn("User creation failed with data = " + userData);
            }
        }

        if (userData.getEcrCurrency() == null) {
            ECRInfo info = VendorUtils.getECRInfo(outboundUserRequest.getEcrId());
            userData.setEcrCurrency(info.getEcrCurrency());
        }

        GameRequest gameRequest = XProGamingOutboundAdaptor.getGameRequest(outboundUserRequest);
        XProGamingOutboundLogger.info("Inside handleMessage: " + gameRequest);

        /*
         * Generate the token and insert into cache
         */

        boolean success = false;

        String ecrId = outboundUserRequest.getEcrId();
        String ecrExternalId = outboundUserRequest.getEcrExternalId();
        String token = null;

        while (!success) {
            token = org.apache.commons.lang.RandomStringUtils.random(XProGamingUtils.getTokenLength(), true, true);
            success = redisTemplate.opsForValue().setIfAbsent(token,
                    ecrId + "/" + ecrExternalId + "/" + gameRequest.getGameID());
            if (success) {
                redisTemplate.expire(token, 10, TimeUnit.MINUTES);
            }
        }

        /*
         * TODO check how long sessionId needs to be persisted. play active session is only for game or is it for all
         * games ?Needs to be confirmed In case if it is for all games use sessionService id . In case if it is for only
         * requested game , this should be persisted based gameId in cache and needs to authenticated for every
         * request.language
         */

        OutboundUserResponse outboundUserResponse =
                XProGamingOutboundAdaptor.populateBasicResponse(outboundUserRequest);
        @SuppressWarnings("unchecked")
        Map<String, Object> vendorParams = (Map<String, Object>) outboundUserResponse.getVendorParams();

        /*
         * based on the client technology (Flash or HTML5) we need to return corresponding URL.
         */
//        String url =VendorConfigCache.getFlashRealGameURL(outboundUserRequest.getProductId(),
//                Vendor.xprogaming.toString(), outboundUserRequest.getPartnerId());
        String language = "";
        int operatorId=XProGamingUtils.getCurrencyCode(userData.getEcrCurrency().toLowerCase());
        String createAccountReq = "operatorId="+operatorId+"&username="+ecrExternalId;
        String accountResp;
        String loginToken=null;
        String securityCode=null;
        language = String.valueOf(outboundUserRequest.getVendorParams().get(VendorConstants.VendorRequestParamKey.language.toString()));
        String loginRequest="operatorId="+operatorId+"&username="+ecrExternalId+"&props=limitsetid:1;ExternalSessionID:"+token+";IsSuperSix:0";
        String hash=null;
        String securityRequest=null;
        try {
        	hash = XProGamingUtils.calculateHash(userData.getEcrCurrency().toLowerCase(),createAccountReq);
        	createAccountReq="accessPassword="+hash+"&"+createAccountReq;
        	accountResp = VendorUtils.sendPostRequest(createAccountReq, XProGamingUtils.getVendorUrl()+"createAccount");
        	XProGamingOutboundLogger.info("account creation response = "+accountResp);
        	JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
        	Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        	StringReader reader = new StringReader(accountResp);
        	Response person = (Response) unmarshaller.unmarshal(reader);
        	XProGamingOutboundLogger.info("account creation response at vendor = "+person);
			hash = XProGamingUtils.calculateHash(userData.getEcrCurrency().toLowerCase(),loginRequest);
			loginRequest="accessPassword="+hash+"&"+loginRequest;
			loginToken = VendorUtils.sendPostRequest(loginRequest, XProGamingUtils.getVendorUrl()+"registerToken");
			XProGamingOutboundLogger.info("registerToken response = "+loginToken);
			reader = new StringReader(loginToken);
        	Response response = (Response) unmarshaller.unmarshal(reader);
        	XProGamingOutboundLogger.info("registerToken response at vendor = "+response);
			loginToken = response.getDescription();
			securityRequest = operatorId+loginToken;
			securityCode = XProGamingUtils.calculateHash(userData.getEcrCurrency().toLowerCase(),securityRequest).toUpperCase();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
        String url = XProGamingUtils.getOpenGameURL();
        StringBuffer sb = new StringBuffer();
        sb.append("gameID="+gameRequest.getGameID());
        sb.append("&operatorID="+operatorId);
        sb.append("&languageID="+XProGamingUtils.getlanguageCode(language.toLowerCase()));
        sb.append("&loginToken="+loginToken);
        sb.append("&securityCode="+securityCode);
//        String secureLogin =VendorConfigCache.getGameServerID(outboundUserRequest.getProductId(), Vendor.xprogaming.toString(), outboundUserRequest.getPartnerId());
        XProGamingOutboundLogger.debug("Constructed URL for  ="+outboundUserRequest.getProductId()+" - " + url+sb.toString());
//        XProGamingOutboundLogger.debug("Constructed secureLogin = " +outboundUserRequest.getProductId()+" - " + secureLogin);

        vendorParams.put(VendorResponseParamKey.STATUS.toString(), VendorConstants.Status.SUCCESS.toString());
        vendorParams.put(VendorResponseParamKey.GAME_URL.toString(), url+sb.toString());
//        vendorParams.put(VendorResponseParamKey.DOMAIN.toString(), url);
//        vendorParams.put(VendorResponseParamKey.TOKEN.toString(), token);
//        vendorParams.put(VendorResponseParamKey.SYMBOL.toString(), gameRequest.getGameID());
//        vendorParams.put(VendorResponseParamKey.TECHNOLOGY.toString(), gameRequest.getTechnology());
//        vendorParams.put(VendorResponseParamKey.PLATFORM.toString(), gameRequest.getClientPlatform());
//        vendorParams.put(VendorResponseParamKey.LANGUAGE.toString(),
//                outboundUserRequest.getVendorParams().get(VendorRequestParamKey.language.toString()));
//        vendorParams.put(VendorResponseParamKey.CASHIERURL.toString(), gameRequest.getCashierURL());
//        vendorParams.put(VendorResponseParamKey.LOBBYURL.toString(), gameRequest.getLobbyURL());
//        vendorParams.put(VendorResponseParamKey.SECURELOGIN.toString(), secureLogin );
        outboundUserResponse.setVendorParams(vendorParams);
        stopWatch.stop();
        return outboundUserResponse;

    }

}
