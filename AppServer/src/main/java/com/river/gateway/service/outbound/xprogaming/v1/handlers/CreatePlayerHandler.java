package com.river.gateway.service.outbound.xprogaming.v1.handlers;

import java.rmi.RemoteException;
import java.util.Map;

import com.river.gateway.service.api.VendorService;
import com.river.gateway.service.beans.UserData;
import com.river.gateway.service.cache.UserDataCache;
import com.river.gateway.service.listener.object.UserLoginData;
import com.river.gateway.service.logger.StubLogger;
import com.river.gateway.service.logger.VendorLogger;
import com.river.gateway.service.message.handlers.IMessageHandler;
import com.river.gateway.service.message.listener.VendorMessageProcessor;
import com.river.gateway.service.messages.common.IMessage;
import com.river.gateway.service.messages.outbound.OutboundUserRequest;
import com.river.gateway.service.messages.outbound.OutboundUserResponse;
import com.river.gateway.service.outbound.xprogaming.v1.XProGamingOutboundAdaptor;
import com.river.gateway.service.outbound.xprogaming.v1.messages.CreatePlayerRequest;
import com.river.gateway.service.outbound.xprogaming.v1.messages.CreateUpdatePlayerResponse;
import com.river.gateway.service.outbound.xprogaming.v1.utils.XProGamingUtils;
import com.river.gateway.service.utils.VendorConstants;
import com.river.gateway.service.utils.VendorConstants.VendorRequestParamKey;
import com.river.gateway.service.utils.VendorUtils;

import biz.gametechclubs.casino.game.external.schemas.CreatePlayerResponse;
import biz.gametechclubs.casino.game.external.schemas.ServiceProxy;

public class CreatePlayerHandler extends AbstractVendorHandler implements IMessageHandler {

    private VendorService vendorService;

    public VendorService getVendorService() {
        return vendorService;
    }

    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    @Override
    public IMessage processOutboundMessage(IMessage message, String correlationId, String replyTo) {
        if (message instanceof OutboundUserRequest) {
            OutboundUserRequest outboundUserRequest = (OutboundUserRequest) message;

            return handleMessage(outboundUserRequest, correlationId, replyTo);
        } else {
            VendorLogger.error("Unknow message {} " + message);
            return null;
        }
    }

    private IMessage handleMessage(OutboundUserRequest outboundUserRequest, String correlationId, String replyTo) {
        VendorLogger.debug("Inside handleMessage: " + outboundUserRequest);
        UserData userData =
                UserDataCache.getUserDataForEcrId(outboundUserRequest.getEcrId(), outboundUserRequest.getVendorId());
        CreateUpdatePlayerResponse createUpdatePlayerResponse = new CreateUpdatePlayerResponse();

        boolean isPlayerCreatedAlready = false;
        Map<String, ? extends Object> vendorParams = outboundUserRequest.getVendorParams();
        
        // for trny, check for trny map id
        String playerIdForTrny = null;
        if (VendorUtils.isTrnyRequest(vendorParams)) {
            playerIdForTrny =
                    UserDataCache.getPlayerIdFromTrnyParticipantId((String) vendorParams
                            .get(VendorConstants.TRNY_PARTICIPANT_ID));
            if (playerIdForTrny != null && !playerIdForTrny.isEmpty()) {
                createUpdatePlayerResponse.setError(XProGamingOutboundAdaptor.SUCCESS_CODE);
                createUpdatePlayerResponse.setPlayerID(Long.valueOf(playerIdForTrny));
                isPlayerCreatedAlready = true;
                VendorLogger.debug("Returning existing trny player :" + createUpdatePlayerResponse);
            }
        } else if (userData != null) {
            if (outboundUserRequest.getVendorId().equals(userData.getVendorId().toString())) {
                createUpdatePlayerResponse.setError(XProGamingOutboundAdaptor.SUCCESS_CODE);
                createUpdatePlayerResponse.setPlayerID(Long.valueOf(userData.getEcrVendorId()));
                isPlayerCreatedAlready = true;
                VendorLogger.debug("Returning existing player :" + createUpdatePlayerResponse);
            }
        }

        if (!isPlayerCreatedAlready) {
        	boolean isStressReq = false;

            CreatePlayerRequest createPlayerRequest =
                    XProGamingOutboundAdaptor.getCreatePlayerRequest(outboundUserRequest);
            VendorLogger.debug("Inside handleMessage: " + createPlayerRequest);

            String gameId = (String) vendorParams.get(VendorRequestParamKey.GAME_ID.name());
            if(gameId != null && gameId.trim().endsWith("STRESS")) {
            	isStressReq = true;
            }

            biz.gametechclubs.casino.game.external.schemas.CreatePlayerResponse response = null;
            try {
            	if(isStressReq) {
                	try {
                		VendorLogger.debug("Sending message to vendor STUB: ");
    					String postResJSON = XProGamingUtils.postRequest(XProGamingUtils.convertObjectToJSON(createPlayerRequest),
    							XProGamingUtils.getCreatePlayerStubUrl());
						response = (CreatePlayerResponse) XProGamingUtils.convertJSONToObject(postResJSON, 
								biz.gametechclubs.casino.game.external.schemas.CreatePlayerResponse.class);
    				} catch (Exception e) {
    					StubLogger.error("Exception getting response from stub : ", e);
    				}
            	} else {
            		VendorLogger.debug("Sending message to vendor: ");
            		ServiceProxy serviceProxy = getServiceProxy(outboundUserRequest);
            		response =
            				serviceProxy
            				.createPlayer(new biz.gametechclubs.casino.game.external.schemas.CreatePlayerRequest(
            						createPlayerRequest.getSecureLogin(), createPlayerRequest.getSecurePassword(),
            						createPlayerRequest.getNickname(), createPlayerRequest.getEmail(),
            						createPlayerRequest.getCurrency(), createPlayerRequest.getLanguage(),
            						createPlayerRequest.getFirstName(), createPlayerRequest.getLastName(),
            						createPlayerRequest.getAccountID(), createPlayerRequest.getBanStatus()));
            	}
            } catch (RemoteException e) {
                VendorLogger.error("Exception in sending request : ", e);
            }

            if (response != null) {
                createUpdatePlayerResponse.setDescription(response.getDescription());
                createUpdatePlayerResponse.setError(response.getError());
                if (response.getPlayerID() != null) {
                    createUpdatePlayerResponse.setPlayerID(response.getPlayerID());
                }
            }

            VendorLogger.debug("Inside create player handleMessage: response :" + createUpdatePlayerResponse);

            if (VendorUtils.isTrnyRequest(vendorParams)) {
                // no need to persist in case of trny participant ids as they are temporary
                String trnyParticipantId = (String) vendorParams.get(VendorConstants.TRNY_PARTICIPANT_ID);

                UserLoginData userLoginData = XProGamingOutboundAdaptor.getUserLoginData(outboundUserRequest);

                UserDataCache.putPlayerIdForTrnyParticipantId(trnyParticipantId,
                        String.valueOf(createUpdatePlayerResponse.getPlayerID()), userLoginData);

                UserData userDataToUpdate = new UserData();
                userDataToUpdate.setEcrCurrency(VendorConstants.PLAY_CHIPS_CCY);
                userDataToUpdate.setEcrExternalId(userLoginData.getEcrExternalID());
                userDataToUpdate.setEcrId(VendorUtils.getEcrIdFromTrnyParticipantId(trnyParticipantId));
                userDataToUpdate.setUserLoginData(userLoginData);

                userDataToUpdate.setEcrVendorId(String.valueOf(createUpdatePlayerResponse.getPlayerID()));
                userDataToUpdate.setVendorId(XProGamingOutboundAdaptor.vendorId.toString());

                UserDataCache.put(userData);

            } else {
                VendorLogger.debug("Updating userdata");
                UserLoginData userLoginData = XProGamingOutboundAdaptor.getUserLoginData(outboundUserRequest);
                // update the cache with playerId and DB
                UserData userDataToUpdate = new UserData();

                if (userLoginData == null || userLoginData.getEcrExternalID() == null) {
                    userLoginData = new UserLoginData();
                    userLoginData.setEcrCurrency(userDataToUpdate.getEcrCurrency());
                    userLoginData.setEcrExternalID(outboundUserRequest.getEcrExternalId());
                    userLoginData.setEcrID(outboundUserRequest.getEcrId());
                    userLoginData.setLabelID(outboundUserRequest.getLabelId());
                    userLoginData.setPartnerID(outboundUserRequest.getPartnerId());
                    userLoginData.setProductID(outboundUserRequest.getProductId());
                }

                userDataToUpdate.setEcrCurrency(userLoginData.getEcrCurrency());
                userDataToUpdate.setEcrExternalId(userLoginData.getEcrExternalID());
                userDataToUpdate.setEcrId(userLoginData.getEcrID());
                userDataToUpdate.setUserLoginData(userLoginData);

                userDataToUpdate.setEcrVendorId(String.valueOf(createUpdatePlayerResponse.getPlayerID()));
                userDataToUpdate.setVendorId(XProGamingOutboundAdaptor.vendorId.toString());
                if (createUpdatePlayerResponse.getPlayerID() > 0) {
                    // update only if playerid > 0 which means returned from vendor
                    vendorService.updateUserData(userDataToUpdate);
                }
            }
        }
        OutboundUserResponse outboundUserResponse =
                XProGamingOutboundAdaptor.getOutboundCreateUpdatePlayerResponse(outboundUserRequest,
                        createUpdatePlayerResponse);

        if (replyTo != null && !replyTo.isEmpty()) {
            VendorMessageProcessor.sendResponseToVendorMessageInvoker(replyTo, correlationId, outboundUserResponse);
            return null;
        } else {
            return outboundUserResponse;
        }
    }
}
