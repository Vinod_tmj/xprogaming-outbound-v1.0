package com.river.gateway.service.outbound.xprogaming.v1.handlers;

import com.river.gateway.service.api.VendorService;
import com.river.gateway.service.cache.VendorConfigCache;
import com.river.gateway.service.logger.VendorLogger;
import com.river.gateway.service.messages.outbound.OutboundRequest;
import com.river.gateway.service.messages.outbound.OutboundUserRequest;
import com.river.gateway.service.utils.VendorUtils;

import biz.gametechclubs.casino.game.external.schemas.ServiceProxy;

public class AbstractVendorHandler {

    protected VendorService vendorService;

    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    private static ServiceProxy SERVICE_PROXY;

    private static String VENDOR_URL;

    public Object sendRequestToVendor(Object request, OutboundUserRequest outboundUserRequest,
            Class<?> responseClassType) {
        Object response = null;
        try {

            String vendorURL =
                    VendorConfigCache.getVendorServiceInfo(outboundUserRequest.getProductId(),
                            outboundUserRequest.getVendorId(), outboundUserRequest.getService(),
                            outboundUserRequest.getApi(), outboundUserRequest.getVersion()).getUrl();

            String responseJson = VendorUtils.postRequest(VendorUtils.convertObjectToJSON(request), vendorURL);

            response = VendorUtils.convertJSONWithDateToObject(responseJson, responseClassType);
        } catch (Exception e) {
            VendorLogger.error("Error in sending user request", e);
        }
        return response;
    }

    public ServiceProxy getServiceProxy(OutboundUserRequest outboundUserRequest) {
        try {
            // Do not create proxy object every time for same vendor url..
            String vendorURL =
                    VendorConfigCache.getVendorServiceInfo(outboundUserRequest.getProductId(),
                            outboundUserRequest.getVendorId(), outboundUserRequest.getService(),
                            outboundUserRequest.getApi(), outboundUserRequest.getVersion()).getUrl();
            if (VENDOR_URL == null) {
                VENDOR_URL = vendorURL;
                VendorLogger.debug("Creating service proxy initially " + vendorURL);
                SERVICE_PROXY = new ServiceProxy(VENDOR_URL);
                return SERVICE_PROXY;
            } else {
                if (vendorURL.equals(VENDOR_URL)) {
                    VendorLogger.debug("Returning existing service proxy for " + vendorURL);
                    return SERVICE_PROXY;
                } else {
                    VendorLogger.debug("Returning new service proxy for " + vendorURL);
                    return new ServiceProxy(vendorURL);
                }
            }

        } catch (Exception e) {
            VendorLogger.error("Error in returning proxy", e);
        }
        return null;
    }

    public ServiceProxy getServiceProxy(OutboundRequest outboundRequest) {
        try {
            // FIXME : do not create proxy object every time for same vendor..
            String vendorURL =
                    VendorConfigCache.getVendorServiceInfo(outboundRequest.getProductId(),
                            outboundRequest.getVendorId(), outboundRequest.getService(), outboundRequest.getApi(),
                            outboundRequest.getVersion()).getUrl();
            return new ServiceProxy(vendorURL);

        } catch (Exception e) {
            VendorLogger.error("Error in returning proxy", e);
        }
        return null;
    }

    public Object sendRequestToVendor(Object request, OutboundRequest outboundRequest, Class<?> responseClassType) {
        Object response = null;
        try {
            String vendorURL =
                    VendorConfigCache.getVendorServiceInfo(outboundRequest.getProductId(),
                            outboundRequest.getVendorId(), outboundRequest.getService(), outboundRequest.getApi(),
                            outboundRequest.getVersion()).getUrl();

            String responseJson = VendorUtils.postRequest(VendorUtils.convertObjectToJSON(request), vendorURL);

            response = VendorUtils.convertJSONWithDateToObject(responseJson, responseClassType);
        } catch (Exception e) {
            VendorLogger.error("Error in sending request", e);
        }
        return response;
    }

}
