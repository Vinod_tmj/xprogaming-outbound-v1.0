package com.river.gateway.service.outbound.xprogaming.v1.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import com.google.gson.Gson;
import com.river.gateway.service.logger.CommonLogger;
import com.river.gateway.service.logger.VendorLogger;
import com.river.gateway.service.outbound.xprogaming.v1.logger.XProGamingOutboundLogger;

public class XProGamingUtils {

	private static String STUB_URL;
	private static String OPEN_GAME_URI;
	private static String CREATE_PLAYER_URI;
	private static int tokenLength;
	private static String openFlashGameURL;
	private static String openHTML5GameURL;
	private static String bankId;
	private static String mode;
	private static String secretCode;
	private static String CREATE_FRB_URL;
	private static String CANCEL_FRB_URL;
	private static String userName;
	private static String vendorUrl;
	private static Map<String,Integer> languageCodeMap = new HashMap<String,Integer>();
	private static Map<String,Integer> currencyCodeMap = new HashMap<String,Integer>();
	private static Map<String,String> secretCodeMap = new HashMap<String,String>();
	
	static{
		languageCodeMap.put("en", 2057);
		languageCodeMap.put("hu", 1038);
		languageCodeMap.put("tr", 1055);
		languageCodeMap.put("it", 1040);
		languageCodeMap.put("el", 1032);
		languageCodeMap.put("es", 3082);
		languageCodeMap.put("fr", 1036);
		languageCodeMap.put("de", 1031);
		languageCodeMap.put("pt", 1046);
		languageCodeMap.put("pt-eu", 1046);
		languageCodeMap.put("ru", 1049);
		languageCodeMap.put("zh", 2052);
		languageCodeMap.put("zh-cn", 2052);
		languageCodeMap.put("jp", 1041);
		languageCodeMap.put("th", 1054);
		languageCodeMap.put("ko", 1042);
		languageCodeMap.put("da", 1030);
		languageCodeMap.put("id", 1057);
		languageCodeMap.put("ko", 1042);
		languageCodeMap.put("nl", 1043);
		currencyCodeMap.put("usd", 222);
		currencyCodeMap.put("eur", 832);
		secretCodeMap.put("eur", "AwkeXjhiuN4K4JBDG43ki4g5nXkdjr");
	}
	
	public static String getSecretCode(String currency){
		String code = secretCodeMap.get(currency);
		return code;
	}
	
	public static int getlanguageCode(String lang){
		Integer code = languageCodeMap.get(lang);
		if(code==null)
			return 2057;
		return code;
	}
	
	public static int getCurrencyCode(String currency){
		Integer code = currencyCodeMap.get(currency);
		if(code==null)
			return 222;
		return code;
	}
	
	public static String getVendorUrl() {
		return vendorUrl;
	}

	public static void setVendorUrl(String vendorUrl) {
		XProGamingUtils.vendorUrl = vendorUrl;
	}

	public static String getUserName() {
		return userName;
	}

	public static void setUserName(String userName) {
		XProGamingUtils.userName = userName;
	}

	public static String getcreateFrbUrl() {
		return CREATE_FRB_URL;
	}

	public static void setcreateFrbUrl(String cREATE_FRB_URL) {
		CREATE_FRB_URL = cREATE_FRB_URL;
	}

	public static String getcancelFrbUrl() {
		return CANCEL_FRB_URL;
	}

	public static void setcancelFrbUrl(String cANCEL_FRB_URL) {
		CANCEL_FRB_URL = cANCEL_FRB_URL;
	}

	public static String getOpenFlashGameURL() {
		return openFlashGameURL;
	}

	public static void setOpenFlashGameURL(String openFlashGameURL) {
		XProGamingUtils.openFlashGameURL = openFlashGameURL;
	}

	public static String getOpenHTML5GameURL() {
		return openHTML5GameURL;
	}

	public static void setOpenHTML5GameURL(String openHTML5GameURL) {
		XProGamingUtils.openHTML5GameURL = openHTML5GameURL;
	}

	public static String getOpenGameURL() {
		return OPEN_GAME_URI;
	}

	public static int getTokenLength() {
		return tokenLength;
	}

	public static void setTokenLength(int tokenLength) {
		XProGamingUtils.tokenLength = tokenLength;
	}


	public static String getSecretCode() {
		return secretCode;
	}

	public static void setSecretCode(String code) {
		secretCode = code;
	}

	public static String getBankId() {
		return bankId;
	}

	public static void setBankId(String bankId) {
		XProGamingUtils.bankId = bankId;
	}

	public static String getMode() {
		return mode;
	}

	public static void setMode(String mode) {
		XProGamingUtils.mode = mode;
	}

	private static Integer httpReadTimeOut = new Integer(15 * 1000);

	private static Gson gson = new Gson();

	public static void setStubUrl(String stubURL) {
		STUB_URL = stubURL;
	}

	public static void setOpenGameUri(String openGameUri) {
		OPEN_GAME_URI = openGameUri;
	}

	public static void setCreatePlayerUri(String createPlayerUri) {
		CREATE_PLAYER_URI = createPlayerUri;
	}

	public static String getOpenGameStubUrl() {
		return STUB_URL + OPEN_GAME_URI;
	}

	public static String getCreatePlayerStubUrl() {
		return STUB_URL + CREATE_PLAYER_URI;
	}

	public static String convertObjectToJSON(Object object) throws Exception {
		String jsonResponse = gson.toJson(object);
		CommonLogger.debug("Output JSON response..... " + jsonResponse);
		return jsonResponse;
	}

	/**
	 * <b>method</b> : convertJSONToObject<BR>
	 * <b>description</b> : Converts JSON Strong into Object of given class type
	 * <BR>
	 * 
	 * @return Object
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object convertJSONToObject(String jsonRequest, Class objectClassType) throws Exception {
		Object object = gson.fromJson(jsonRequest, objectClassType);
		return object;
	}

	public static String postRequest(String jsonRequest, String url) {
		String jsonResponse = null;
		try {
			VendorLogger.info("Sending 'POST' request -->" + jsonRequest + "|URL:" + url);
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			// Send post request
			sendPostRequest(con, jsonRequest);

			int responseCode = con.getResponseCode();
			VendorLogger.debug("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer outputResponse = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				outputResponse.append(inputLine);
			}
			in.close();
			jsonResponse = outputResponse.toString();
		} catch (Exception e) {
			VendorLogger.error("Exception in getting response :", e);
		}
		return jsonResponse;
	}

	public static void sendPostRequest(HttpURLConnection con, String jsonData) throws Exception {
		// con.setReadTimeout(httpReadTimeOut);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(jsonData);
		wr.flush();
		wr.close();
	}

	public static String calculateHash(String cur, String request) throws NoSuchAlgorithmException, InvalidKeyException {
		request = getSecretCode()+request;
		String calculatedHmac = DatatypeConverter.printHexBinary(encode(request, "MD5"));
		return calculatedHmac;
	}

	public static byte[] encode(String text, String algorithm) {

		MessageDigest md;
		if (text == null) {
			return new byte[] {};
		}
		try {
			md = MessageDigest.getInstance(algorithm);
			md.update(text.getBytes("UTF-8"), 0, text.length());
		} catch (NoSuchAlgorithmException e) {
			XProGamingOutboundLogger.error("There is no " + algorithm + " algorithm avaliable.", e);
			return new byte[] {};
		} catch (UnsupportedEncodingException e) {
			XProGamingOutboundLogger.error("Could not encoding with UTF-8 " + text + ". ", e);
			return new byte[] {};
		}
		return md.digest();
	}

}
