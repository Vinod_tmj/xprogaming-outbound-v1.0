package com.river.gateway.service.outbound.xprogaming.v1.messages;


/**
 * 
 * @author Dilip Reddy
 * 
 */
public class GameRequest extends CommonOutboundRequest {

    /**
     * Unique identifier for the game provided by Topgame system. The game library will be sent together with the
     * integration package. Example: vs25a
     */
    private String gameID;

    /**
     * ID of End User on the side of Topgame system. The Operator will receive this ID in response to CreatePlayer
     * method. Example: 182226
     */
    private long playerID;

    /**
     * Technology platform of the selected game. Games may be implemented in a number of technologies. <br>
     * Possible values: H5 - HTML5 F - FLASH U - Unity <br>
     * If this parameter is not defined in the game open request, the default Flash based version of the game will open.
     * The Game Library stores information on the technologies supported by specific games.
     */
    private String technology;

    /**
     * Client platform for the selected game. The game will open in the appropriate mode based on the supplied client
     * platform. <br>
     * Possible values: MOBILE - game mode for mobile devices WEB - game mode for desktop devices.<br>
     * If this parameter is not defined in the game open request, the game will open in the default WEB mode for a
     * desktop device. The Game Library stores information on the client platforms supported by specific games.
     */
    private String clientPlatform;

    /**
     * Link for opening the Cashier when an End User does not have any funds.
     */
    private String cashierURL;

    /**
     * Link for opening Operator's website Lobby page. <br>
     * This link is accessed from the Back to Lobby button available in the mobile version of the Unity3D and HTML5
     * games.
     */
    private String bankId;
    private String mode;
    
    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    private String lobbyURL;

    public String getGameID() {
        return gameID;
    }

    public void setGameID(String gameID) {
        this.gameID = gameID;
    }

    public long getPlayerID() {
        return playerID;
    }

    public void setPlayerID(long playerID) {
        this.playerID = playerID;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getClientPlatform() {
        return clientPlatform;
    }

    public void setClientPlatform(String clientPlatform) {
        this.clientPlatform = clientPlatform;
    }

    public String getCashierURL() {
        return cashierURL;
    }

    public void setCashierURL(String cashierURL) {
        this.cashierURL = cashierURL;
    }

    public String getLobbyURL() {
        return lobbyURL;
    }

    public void setLobbyURL(String lobbyURL) {
        this.lobbyURL = lobbyURL;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GameRequest [gameID=");
        builder.append(gameID);
        builder.append(", playerID=");
        builder.append(playerID);
        builder.append(", technology=");
        builder.append(technology);
        builder.append(", clientPlatform=");
        builder.append(clientPlatform);
        builder.append(", cashierURL=");
        builder.append(cashierURL);
        builder.append(", lobbyURL=");
        builder.append(lobbyURL);
        builder.append(", getSecureLogin()=");
        builder.append(getSecureLogin());
        builder.append(", getSecurePassword()=");
        builder.append(getSecurePassword());
        builder.append("]");
        return builder.toString();
    }

}