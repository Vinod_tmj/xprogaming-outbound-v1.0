package com.river.gateway.service.outbound.xprogaming.v1.messages;

public class CommonOutboundResponse {

    /**
     * 0 - if the End User registered successfully; Error code - in other circumstances.
     */
    private String error;

    /**
     * Description of the error for troubleshooting purposes.
     */
    private String description;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CommonOutboundResponse [error=");
        builder.append(error);
        builder.append(", description=");
        builder.append(description);
        builder.append("]");
        return builder.toString();
    }

}
