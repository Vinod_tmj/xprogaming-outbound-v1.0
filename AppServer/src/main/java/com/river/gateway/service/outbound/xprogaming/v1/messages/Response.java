package com.river.gateway.service.outbound.xprogaming.v1.messages;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="response", namespace="apiResultData")
public class Response {
	
	private int errorCode;
	private String description;

	public int getErrorCode() {
		return errorCode;
	}
	
	@XmlElement(name="errorCode")
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getDescription() {
		return description;
	}
	
	@XmlElement(name="description")
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Response [errorCode=");
		builder.append(errorCode);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}
}
