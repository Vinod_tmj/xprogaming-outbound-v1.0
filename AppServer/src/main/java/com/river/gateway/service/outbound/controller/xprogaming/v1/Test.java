package com.river.gateway.service.outbound.controller.xprogaming.v1;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.river.gateway.service.outbound.xprogaming.v1.logger.XProGamingOutboundLogger;
import com.river.gateway.service.outbound.xprogaming.v1.messages.Response;

public class Test {

	static String secretCode = "AwkeXjhiuN4K4JBDG43ki4g5nXkdjr";
	public static void main(String[] args) {
		String request = "operatorId="+832+"&username="+12345;
		try {
			String accessPassword = calculateHash(request);
			request = "accessPassword="+accessPassword+"&"+request;
			String loginToken = "<?xml version=\"1.0\" encoding=\"utf-8\"?><response xmlns=\"apiResultData\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><errorCode>3</errorCode><description/></response>";
			JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
        	Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        	StringReader reader = new StringReader(loginToken);
        	Response person = (Response) unmarshaller.unmarshal(reader);
			System.out.println(person);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String calculateHash(String request) throws NoSuchAlgorithmException, InvalidKeyException {
		request = secretCode+request;
		String calculatedHmac = DatatypeConverter.printHexBinary(encode(request, "MD5"));
		return calculatedHmac;
	}

	public static byte[] encode(String text, String algorithm) {

		MessageDigest md;
		if (text == null) {
			return new byte[] {};
		}
		try {
			md = MessageDigest.getInstance(algorithm);
			md.update(text.getBytes("UTF-8"), 0, text.length());
		} catch (NoSuchAlgorithmException e) {
			XProGamingOutboundLogger.error("There is no " + algorithm + " algorithm avaliable.", e);
			return new byte[] {};
		} catch (UnsupportedEncodingException e) {
			XProGamingOutboundLogger.error("Could not encoding with UTF-8 " + text + ". ", e);
			return new byte[] {};
		}
		return md.digest();
	}

}
