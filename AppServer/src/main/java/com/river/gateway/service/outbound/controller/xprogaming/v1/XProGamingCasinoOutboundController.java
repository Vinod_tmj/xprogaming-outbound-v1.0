package com.river.gateway.service.outbound.controller.xprogaming.v1;

import org.perf4j.LoggingStopWatch;
import org.perf4j.StopWatch;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.river.gateway.service.logger.VendorLogger;
import com.river.gateway.service.messages.outbound.OutboundUserRequest;
import com.river.gateway.service.messages.outbound.OutboundUserResponse;
import com.river.gateway.service.outbound.xprogaming.v1.logger.XProGamingOutboundLogger;
import com.river.gateway.service.registry.MessageHandlerRegistry;

/**
 * 
 * Requests from Casino Service
 * 
 * @author vinod
 * 
 */
@Controller
@RequestMapping({"/xprogaming"})
public class XProGamingCasinoOutboundController {

    public XProGamingCasinoOutboundController() {
        init();
    }

    public void init() {
        VendorLogger.info(this.getClass().getSimpleName() + " loaded");
    }

    @RequestMapping(value = {"/createPlayer"}, method = {RequestMethod.POST})
    @ResponseBody
    public OutboundUserResponse createPlayer(@RequestBody OutboundUserRequest request) {
        VendorLogger.info("In XProGamingCasinoOutboundController createPlayer:: ");
        OutboundUserResponse response =
                (OutboundUserResponse) MessageHandlerRegistry.delegateOutboundUserMessage(request, null, null);
        VendorLogger.info("In XProGamingCasinoOutboundController returning createPlayer:: " + response);
        return response;
    }

    @RequestMapping(value = {"/updatePlayer"}, method = {RequestMethod.POST})
    @ResponseBody
    public OutboundUserResponse updatePlayer(@RequestBody OutboundUserRequest request) {
        VendorLogger.info("In XProGamingCasinoOutboundController updatePlayer:: ");
        OutboundUserResponse response =
                (OutboundUserResponse) MessageHandlerRegistry.delegateOutboundUserMessage(request, null, null);
        VendorLogger.info("In XProGamingCasinoOutboundController returning updatePlayer:: " + response);
        return response;
    }

    @RequestMapping(value = {"/getPlayer"}, method = {RequestMethod.POST})
    @ResponseBody
    public OutboundUserResponse getPlayer(@RequestBody OutboundUserRequest request) {
        VendorLogger.info("In XProGamingCasinoOutboundController getPlayer:: ");
        OutboundUserResponse response =
                (OutboundUserResponse) MessageHandlerRegistry.delegateOutboundUserMessage(request, null, null);
        VendorLogger.info("In XProGamingCasinoOutboundController returning getPlayer:: " + response);
        return response;
    }

    @RequestMapping(value = {"/openGame"}, method = {RequestMethod.POST})
    @ResponseBody
    public OutboundUserResponse openGame(@RequestBody OutboundUserRequest request) {
        StopWatch stopWatch = new LoggingStopWatch("openGame-XProGamingCasinoOutboundController");
        XProGamingOutboundLogger.info("In XProGamingCasinoOutboundController openGame:: ");
        OutboundUserResponse response =
                (OutboundUserResponse) MessageHandlerRegistry.delegateOutboundUserMessage(request, null, null);
        XProGamingOutboundLogger.info("In XProGamingCasinoOutboundController returning openGame:: " + response);
        stopWatch.stop();
        return response;
    }
    
//    @RequestMapping(value = {"/createFRB"}, method = {RequestMethod.POST})
//    @ResponseBody
//    public Boolean createFRB(@RequestBody FreeSpinData request) {
//    	Boolean response=false;
//    	XProGamingOutboundLogger.info("In XProGamingCasinoOutboundController createFRB:: "+request);
//    	long timestamp=0l;
//    	try{
//    		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
//    		Date parsedDate = dateFormat.parse(request.getExpiryDate());
//    		timestamp = parsedDate.getTime()/1000;
//    		Calendar cal = Calendar.getInstance();
//    		cal.add(Calendar.DATE, 30);
//    		if(timestamp>cal.getTimeInMillis()/1000)
//    			timestamp=cal.getTimeInMillis()/1000;
//    	}catch(Exception e){
//    		e.printStackTrace();
//    	}
//    	String url = XProGamingUtils.getcreateFrbUrl();
//    	String req = "bonusCode="+request.getBonusId()+"&currency="+VendorUtils.getECRInfo(request.getEcrId()).getEcrCurrency()
//    			+"&expirationDate="+timestamp+"&gameIDList="+request.getGameList()
//    			+"&playerId="+VendorUtils.getExternalIdForEcrId(request.getEcrId())+"&rounds="+request.getNoOfSpins()
//    			+"&secureLogin="+XProGamingUtils.getUserName();
//    	String hash="test";
//    	try {
//			hash = XProGamingUtils.calculateHash(req);
//		} catch (InvalidKeyException e1) {
//			e1.printStackTrace();
//		} catch (NoSuchAlgorithmException e1) {
//			e1.printStackTrace();
//		}
//    	String params = "secureLogin="+XProGamingUtils.getUserName()+"&playerId="+VendorUtils.getExternalIdForEcrId(request.getEcrId())
//    	+"&currency="+VendorUtils.getECRInfo(request.getEcrId()).getEcrCurrency()
//    	+"&gameIDList="+request.getGameList()+"&rounds="+request.getNoOfSpins()+"&bonusCode="+request.getBonusId()
//    	+"&expirationDate="+timestamp+"&hash="+hash;
//    	XProGamingOutboundLogger.info("Prepared request = "+url+params);
//    	try {
//			String result = VendorUtils.sendGetRequest(url+params);
//			CreateFRBResponse createFRBResponse = (CreateFRBResponse) VendorUtils.convertJSONToObject(result,
//					CreateFRBResponse.class);
//			XProGamingOutboundLogger.info("createFRBResponse = "+createFRBResponse);
//			if(createFRBResponse.getError().equals("0"))
//				response=true;
//		}catch (Exception e) {
//			XProGamingOutboundLogger.error("Exception in createFRB: ", e);
//			e.printStackTrace();
//		}
//    	XProGamingOutboundLogger.info("In XProGamingCasinoOutboundController returning createFRB:: " + response);
//        return response;
//    }
//    
//    @RequestMapping(value = {"/dropFRB"}, method = {RequestMethod.POST})
//    @ResponseBody
//    public Boolean dropFRB(@RequestBody FreeSpinData request) {
//    	Boolean response=false;
//    	XProGamingOutboundLogger.info("In XProGamingCasinoOutboundController dropFRB:: "+request);
//    	String url = XProGamingUtils.getcancelFrbUrl();
//    	String req = "bonusCode="+request.getBonusId()+"&secureLogin="+XProGamingUtils.getUserName();
//    	String hash="test";
//    	try {
//			hash = XProGamingUtils.calculateHash(req);
//		} catch (InvalidKeyException e1) {
//			e1.printStackTrace();
//		} catch (NoSuchAlgorithmException e1) {
//			e1.printStackTrace();
//		}
//    	String params = "secureLogin="+XProGamingUtils.getUserName()+"&bonusCode="+request.getBonusId()+"&hash="+hash;
//    	XProGamingOutboundLogger.info("Prepared request = "+url+params);
//    	try {
//			String result = VendorUtils.sendGetRequest(url+params);
//			DropFrBResponse dropFrBResponse = (DropFrBResponse) VendorUtils.convertJSONToObject(result,
//					DropFrBResponse.class);
//			XProGamingOutboundLogger.info("dropFrBResponse = "+dropFrBResponse);
//			if(dropFrBResponse.getError().equals("0"))
//				response=true;
//		}catch (Exception e) {
//			XProGamingOutboundLogger.error("Exception in dropFRB: ", e);
//			e.printStackTrace();
//		}
//    	XProGamingOutboundLogger.info("In XProGamingCasinoOutboundController returning dropFRB:: " + response);
//        return response;
//    }

}