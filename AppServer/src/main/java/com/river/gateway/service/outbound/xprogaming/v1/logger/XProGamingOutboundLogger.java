package com.river.gateway.service.outbound.xprogaming.v1.logger;

import org.apache.log4j.Logger;

/**
 * 
 * @author vamsiswaroop Reddy
 * 
 */
public class XProGamingOutboundLogger {

    private final static Logger XPROGAMING_OUTBOUND_LOGGER = Logger.getLogger("XPROGAMING_OUTBOUND_LOGGER");

    public static void warn(String message) {
        XPROGAMING_OUTBOUND_LOGGER.warn(message);
    }

    public static void warn(String message, Throwable t) {
        XPROGAMING_OUTBOUND_LOGGER.warn(message, t);
    }

    public static void error(String message) {
        XPROGAMING_OUTBOUND_LOGGER.error(message);
    }

    public static void error(String message, Throwable t) {
        XPROGAMING_OUTBOUND_LOGGER.error(message, t);
    }

    public static void fatal(String message) {
        XPROGAMING_OUTBOUND_LOGGER.fatal(message);
    }

    public static void fatal(String message, Throwable t) {
        XPROGAMING_OUTBOUND_LOGGER.fatal(message, t);
    }

    public static void info(String message) {
        if (XPROGAMING_OUTBOUND_LOGGER.isInfoEnabled()) {
            XPROGAMING_OUTBOUND_LOGGER.info(message);
        }
    }

    public static void info(String message, Throwable t) {
        if (XPROGAMING_OUTBOUND_LOGGER.isInfoEnabled()) {
            XPROGAMING_OUTBOUND_LOGGER.info(message, t);
        }
    }

    public static void debug(String message) {
        if (XPROGAMING_OUTBOUND_LOGGER.isDebugEnabled()) {
            XPROGAMING_OUTBOUND_LOGGER.debug(message);
        }
    }

    public static void debug(String message, Throwable t) {
        if (XPROGAMING_OUTBOUND_LOGGER.isDebugEnabled()) {
            XPROGAMING_OUTBOUND_LOGGER.debug(message, t);
        }
    }

}
