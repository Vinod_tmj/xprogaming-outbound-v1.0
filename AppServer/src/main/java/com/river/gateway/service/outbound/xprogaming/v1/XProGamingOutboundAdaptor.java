package com.river.gateway.service.outbound.xprogaming.v1;

import java.util.HashMap;
import java.util.Map;

import org.perf4j.LoggingStopWatch;
import org.perf4j.StopWatch;

import com.river.gateway.service.beans.UserData;
import com.river.gateway.service.cache.UserDataCache;
import com.river.gateway.service.cache.VendorConfigCache;
import com.river.gateway.service.integration.ecr.UserProfile;
import com.river.gateway.service.integration.fund.object.ECRInfo;
import com.river.gateway.service.listener.object.UserLoginData;
import com.river.gateway.service.logger.VendorLogger;
import com.river.gateway.service.messages.common.MessageType;
import com.river.gateway.service.messages.outbound.OutboundUserRequest;
import com.river.gateway.service.messages.outbound.OutboundUserResponse;
import com.river.gateway.service.outbound.xprogaming.v1.messages.CreatePlayerRequest;
import com.river.gateway.service.outbound.xprogaming.v1.messages.CreateUpdatePlayerResponse;
import com.river.gateway.service.outbound.xprogaming.v1.messages.GameRequest;
import com.river.gateway.service.outbound.xprogaming.v1.messages.GameResponse;
import com.river.gateway.service.outbound.xprogaming.v1.messages.GetPlayerRequest;
import com.river.gateway.service.outbound.xprogaming.v1.messages.GetPlayerResponse;
import com.river.gateway.service.outbound.xprogaming.v1.messages.UpdatePlayerRequest;
import com.river.gateway.service.registry.MessageHandlerRegistry;
import com.river.gateway.service.utils.VendorConstants;
import com.river.gateway.service.utils.VendorConstants.Vendor;
import com.river.gateway.service.utils.VendorConstants.VendorProduct;
import com.river.gateway.service.utils.VendorConstants.VendorRequestParamKey;
import com.river.gateway.service.utils.VendorConstants.VendorResponseParamKey;
import com.river.gateway.service.utils.VendorInfo;
import com.river.gateway.service.utils.VendorUtils;

/**
 * 
 * @author Dilip Reddy
 * 
 */
public class XProGamingOutboundAdaptor {

    public static Vendor vendorId = Vendor.xprogaming;

    public static String SUCCESS_CODE = "0";

    public static String DUPLICATE_CODE = "3";

    public static String DEFAULT_FAILURE_CODE = "TECHNICAL_ERROR";

    public static final String PRODUCT_ID = VendorProduct.CASINO.toString();

    public static UserLoginData getUserLoginData(OutboundUserRequest outboundUserRequest) {
        return UserDataCache.getLoggedInUser(outboundUserRequest.getEcrId());
    }

    public static OutboundUserResponse populateBasicResponse(OutboundUserRequest outboundUserRequest) {
        StopWatch stopWatch = new LoggingStopWatch("openGame-GM-populateBasicResponse");
        OutboundUserResponse outboundUserResponse = new OutboundUserResponse();
        outboundUserResponse.setApi(outboundUserRequest.getApi());
        outboundUserResponse.setEcrId(outboundUserRequest.getEcrId());
        outboundUserResponse.setLabelId(outboundUserRequest.getLabelId());
        outboundUserResponse.setMessageName(outboundUserRequest.getMessageName());
        outboundUserResponse.setMessageType(outboundUserRequest.getMessageType());
        outboundUserResponse.setPartnerId(outboundUserRequest.getPartnerId());
        outboundUserResponse.setProductId(outboundUserRequest.getProductId());
        outboundUserResponse.setService(outboundUserRequest.getService());
        outboundUserResponse.setSessionKey(outboundUserRequest.getSessionKey());
        outboundUserResponse.setEcrExternalId(outboundUserRequest.getEcrExternalId());
        outboundUserResponse.setVendorId(outboundUserRequest.getVendorId());
        outboundUserResponse.setVersion(outboundUserRequest.getVersion());

        Map<String, Object> vendorParams = new HashMap<String, Object>();

        vendorParams.put(VendorResponseParamKey.SESSION_KEY.toString(), outboundUserRequest.getSessionKey());
        outboundUserResponse.setVendorParams(vendorParams);
        stopWatch.stop();
        return outboundUserResponse;
    
    }

    public static CreatePlayerRequest getCreatePlayerRequest(OutboundUserRequest outboundUserRequest) {
        CreatePlayerRequest createPlayerRequest = new CreatePlayerRequest();

        Map<String, ? extends Object> vendorParams = outboundUserRequest.getVendorParams();

        // for trny, check for trny map id
        if (VendorUtils.isTrnyRequest(vendorParams)) {
            UserLoginData userLoginData = getUserLoginData(outboundUserRequest);
            String trnyParticipantId = (String) vendorParams.get(VendorConstants.TRNY_PARTICIPANT_ID);
            // trnyParticipantId will be in the format TRNY-<trnyId>-<ecrId>;
            String trnyId = VendorUtils.getTrnyIdFromTrnyParticipantId(trnyParticipantId);
            String accountIdAtVendor = "TRUSR" + userLoginData.getEcrExternalID() + trnyId;

            createPlayerRequest.setAccountID(accountIdAtVendor);
            createPlayerRequest.setCurrency(VendorConstants.PLAY_CHIPS_CCY);
            createPlayerRequest.setEmail(userLoginData.getEmailID());
            // createPlayerRequest.setEmail("asdf1@techmojo.in");
            // createPlayerRequest.setCurrency("PHP");

            createPlayerRequest.setNickname("T-" + userLoginData.getEmailID() + "-" + trnyId);

            createPlayerRequest.setLanguage(VendorConstants.DEFAULT_LANGUAGE);
        } else {
            UserLoginData userLoginData = getUserLoginData(outboundUserRequest);
            if (userLoginData != null && userLoginData.getEcrExternalID() != null) {
                createPlayerRequest.setAccountID(userLoginData.getEcrExternalID());
                createPlayerRequest.setCurrency(userLoginData.getEcrCurrency());
                createPlayerRequest.setEmail(userLoginData.getEmailID());
                createPlayerRequest.setNickname(userLoginData.getEcrExternalID());
                createPlayerRequest.setLanguage(VendorConstants.DEFAULT_LANGUAGE);
            } else {
                UserProfile userProfile = VendorUtils.getUserProfileFromCache(outboundUserRequest.getEcrId());
                createPlayerRequest.setAccountID(outboundUserRequest.getEcrExternalId());
                ECRInfo ecrInfo = VendorUtils.getECRInfo(outboundUserRequest.getEcrId());
                createPlayerRequest.setCurrency(ecrInfo.getEcrCurrency());
                createPlayerRequest.setEmail(userProfile.getEmailId());
                createPlayerRequest.setNickname(outboundUserRequest.getEcrExternalId());
                createPlayerRequest.setLanguage(VendorConstants.DEFAULT_LANGUAGE);
            }
        }

        createPlayerRequest.setBanStatus(false);
        createPlayerRequest.setFirstName("");
        createPlayerRequest.setLastName("");

        VendorInfo vendorInfo =
                VendorConfigCache.getVendorInfo(outboundUserRequest.getProductId(), outboundUserRequest.getVendorId());

        createPlayerRequest.setSecureLogin(vendorInfo.getLoginId());
        createPlayerRequest.setSecurePassword(vendorInfo.getPassKey());

        return createPlayerRequest;
    }

    public static UpdatePlayerRequest getUpdatePlayerRequest(OutboundUserRequest outboundUserRequest) {
        UpdatePlayerRequest updatePlayerRequest = new UpdatePlayerRequest();

        UserLoginData userLoginData = getUserLoginData(outboundUserRequest);

        updatePlayerRequest.setFirstName("");
        updatePlayerRequest.setBanStatus(false);
        updatePlayerRequest.setEmail(userLoginData.getEmailID());
        updatePlayerRequest.setFirstName("");
        updatePlayerRequest.setLanguage("en");// FIXME : not present in ECR profile yet
        updatePlayerRequest.setLastName("");
        updatePlayerRequest.setNickname(userLoginData.getUsername());

        UserData userData = UserDataCache.getUserDataForEcrId(userLoginData.getEcrID(), vendorId.toString());

        updatePlayerRequest.setPlayerID(Long.valueOf(userData.getEcrVendorId()));

        VendorInfo vendorInfo =
                VendorConfigCache.getVendorInfo(outboundUserRequest.getProductId(), outboundUserRequest.getVendorId());

        updatePlayerRequest.setSecureLogin(vendorInfo.getLoginId());
        updatePlayerRequest.setSecurePassword(vendorInfo.getPassKey());

        return updatePlayerRequest;
    }

    public static OutboundUserResponse getOutboundCreateUpdatePlayerResponse(OutboundUserRequest outboundUserRequest,
            CreateUpdatePlayerResponse createUpdatePlayerResponse) {
        OutboundUserResponse outboundUserResponse = populateBasicResponse(outboundUserRequest);

        @SuppressWarnings("unchecked")
        Map<String, Object> vendorParams = (Map<String, Object>) outboundUserResponse.getVendorParams();

        if (createUpdatePlayerResponse.getError().equals(SUCCESS_CODE)
                || createUpdatePlayerResponse.getError().equals(DUPLICATE_CODE)) {
            /* even if duplicate also consider as success.. as they would have returned player id */
            vendorParams.put(VendorResponseParamKey.STATUS.toString(), VendorConstants.Status.SUCCESS.toString());
            vendorParams.put(VendorResponseParamKey.ERROR_DESC.toString(), createUpdatePlayerResponse.getDescription());
        } else {
            vendorParams.put(VendorResponseParamKey.STATUS.toString(), VendorConstants.Status.FAILURE.toString());
            vendorParams.put(VendorResponseParamKey.ERROR_DESC.toString(), createUpdatePlayerResponse.getDescription());
        }

        vendorParams.put(VendorResponseParamKey.PLAYER_ID.toString(), createUpdatePlayerResponse.getPlayerID());

        outboundUserResponse.setVendorParams(vendorParams);
        return outboundUserResponse;
    }

    public static GetPlayerRequest getPlayerRequest(OutboundUserRequest outboundUserRequest) {
        GetPlayerRequest getPlayerRequest = new GetPlayerRequest();

        UserLoginData userLoginData = getUserLoginData(outboundUserRequest);
        UserData userData = UserDataCache.getUserDataForEcrId(userLoginData.getEcrID(), vendorId.toString());

        getPlayerRequest.setPlayerID(Long.valueOf(userData.getEcrVendorId()));

        VendorInfo vendorInfo =
                VendorConfigCache.getVendorInfo(outboundUserRequest.getProductId(), outboundUserRequest.getVendorId());

        getPlayerRequest.setSecureLogin(vendorInfo.getLoginId());
        getPlayerRequest.setSecurePassword(vendorInfo.getPassKey());

        return getPlayerRequest;
    }

    public static OutboundUserResponse getOutboundPlayerResponse(OutboundUserRequest outboundUserRequest,
            GetPlayerResponse getPlayerResponse) {
        OutboundUserResponse outboundUserResponse = populateBasicResponse(outboundUserRequest);

        @SuppressWarnings("unchecked")
        Map<String, Object> vendorParams = (Map<String, Object>) outboundUserResponse.getVendorParams();

        vendorParams.put(VendorResponseParamKey.STATUS.toString(), VendorConstants.Status.SUCCESS.toString());
        vendorParams.put(VendorResponseParamKey.PLAYER_ID.toString(), getPlayerResponse.getPlayerID());
        vendorParams.put(VendorResponseParamKey.BAN_STATUS.toString(), getPlayerResponse.getBanStatus());

        outboundUserResponse.setVendorParams(vendorParams);
        return outboundUserResponse;
    }

    public static GameRequest getGameRequest(OutboundUserRequest outboundUserRequest) {
        StopWatch stopWatch = new LoggingStopWatch("openGame-GM-XProGamingOutboundAdaptor-");
        VendorLogger.debug("outboundUserRequest in Adaptor : " + outboundUserRequest);

        GameRequest gameRequest = new GameRequest();

        Map<String, ? extends Object> vendorParams = outboundUserRequest.getVendorParams();
        if (vendorParams != null) {
            String gameID = (String) vendorParams.get(VendorRequestParamKey.GAME_ID.toString());
            String clientPlatform = (String) vendorParams.get(VendorRequestParamKey.CLIENT_PLATFORM.toString());
            String technologyPlatform = (String) vendorParams.get(VendorRequestParamKey.TECHNOLOGY_PLATFORM.toString());

            gameRequest.setGameID(gameID);
            gameRequest.setClientPlatform(clientPlatform);
            gameRequest.setTechnology(technologyPlatform);
        }

        gameRequest.setCashierURL(VendorUtils.getCashierURL());
        gameRequest.setLobbyURL(VendorUtils.getCasinoLobbyURL());

        String ecrId = outboundUserRequest.getEcrId();

        UserData userData = UserDataCache.getUserDataForEcrId(ecrId, vendorId.toString());

        long playerId;

        if (VendorUtils.isTrnyRequest(vendorParams)) {
            // if request is from tourney, check if playerId created at vendor side for this participant Id, if not
            // create it and use that playerId
            String playerIdForTrny = UserDataCache
                    .getPlayerIdFromTrnyParticipantId((String) vendorParams.get(VendorConstants.TRNY_PARTICIPANT_ID));

            if (playerIdForTrny == null) {
                // create
                VendorUtils.sendCreatePlayerRequestForTrny(outboundUserRequest);
                playerIdForTrny = UserDataCache.getPlayerIdFromTrnyParticipantId(
                        (String) vendorParams.get(VendorConstants.TRNY_PARTICIPANT_ID));
            }
            playerId = Long.valueOf(playerIdForTrny);
            // now again get from Cache
        } else {
            // if no userData create Player and then try
            if (userData == null) {
                VendorLogger.debug("Player mapping does not exist, try creating now for : " + outboundUserRequest);
                OutboundUserRequest request = new OutboundUserRequest();
                request.setApi("createPlayer");
                request.setEcrId(ecrId);
                request.setEcrExternalId(outboundUserRequest.getEcrExternalId());
                request.setLabelId(outboundUserRequest.getLabelId());
                request.setMessageName("CreatePlayerRequest");
                request.setMessageType(MessageType.USER);
                request.setPartnerId(outboundUserRequest.getPartnerId());
                request.setProductId(outboundUserRequest.getProductId());
                request.setService("userProfile");
                request.setSessionKey(outboundUserRequest.getSessionKey());
                request.setVendorId(VendorConstants.Vendor.xprogaming.toString());
                request.setVendorParams(vendorParams);
                request.setVersion("1.0");
                MessageHandlerRegistry.delegateOutboundUserMessage(request, null, null);
                VendorLogger.debug("Create player request sent");
                userData = UserDataCache.getUserDataForEcrId(ecrId, vendorId.toString());
            }
            playerId = Long.valueOf(userData.getEcrVendorId());
        }

        gameRequest.setPlayerID(playerId);

        VendorInfo vendorInfo =
                VendorConfigCache.getVendorInfo(outboundUserRequest.getProductId(), outboundUserRequest.getVendorId());

        gameRequest.setSecureLogin(vendorInfo.getLoginId());
        gameRequest.setSecurePassword(vendorInfo.getPassKey());
        stopWatch.stop();
        return gameRequest;
    }

    public static OutboundUserResponse getOutboundGameResponse(OutboundUserRequest outboundUserRequest,
            GameResponse gameResponse) {
        OutboundUserResponse outboundUserResponse = populateBasicResponse(outboundUserRequest);

        @SuppressWarnings("unchecked")
        Map<String, Object> vendorParams = (Map<String, Object>) outboundUserResponse.getVendorParams();

        if (gameResponse.getError().equals(SUCCESS_CODE)) {
            vendorParams.put(VendorResponseParamKey.STATUS.toString(), VendorConstants.Status.SUCCESS.toString());
        } else {
            vendorParams.put(VendorResponseParamKey.STATUS.toString(), VendorConstants.Status.FAILURE.toString());
            vendorParams.put(VendorResponseParamKey.ERROR_DESC.toString(), gameResponse.getDescription());
        }

        vendorParams.put(VendorResponseParamKey.GAME_URL.toString(), gameResponse.getUrl());

        outboundUserResponse.setVendorParams(vendorParams);
        return outboundUserResponse;
    }

}
