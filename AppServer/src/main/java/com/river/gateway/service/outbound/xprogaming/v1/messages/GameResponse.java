package com.river.gateway.service.outbound.xprogaming.v1.messages;

/**
 * 
 * @author Dilip Reddy
 * 
 */
public class GameResponse extends CommonOutboundResponse {

    /**
     * A link to the game on the side of Topgame system. <br>
     * This link will contain special parameters which should remain as they appear. <br>
     * Example: https:// domain.biz/gs2c/game.jsp?tc= 6wXLlLbJAr4IUQ0XCUpJ6lPxkkvfWd2H
     */
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GameResponse [url=");
        builder.append(url);
        builder.append(", error=");
        builder.append(getError());
        builder.append(", description=");
        builder.append(getDescription());
        builder.append("]");
        return builder.toString();
    }

}