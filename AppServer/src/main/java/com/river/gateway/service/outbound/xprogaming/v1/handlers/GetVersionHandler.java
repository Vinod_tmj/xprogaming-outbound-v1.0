package com.river.gateway.service.outbound.xprogaming.v1.handlers;

import java.rmi.RemoteException;

import com.river.gateway.service.logger.VendorLogger;
import com.river.gateway.service.message.handlers.IMessageHandler;
import com.river.gateway.service.message.listener.VendorMessageProcessor;
import com.river.gateway.service.messages.common.IMessage;
import com.river.gateway.service.messages.outbound.OutboundRequest;
import com.river.gateway.service.outbound.xprogaming.v1.messages.GetVersionResponse;

import biz.gametechclubs.casino.game.external.schemas.ServiceProxy;
import biz.gametechclubs.casino.game.external.schemas.VersionRequest;

public class GetVersionHandler extends AbstractVendorHandler implements IMessageHandler {

    @Override
    public IMessage processOutboundMessage(IMessage message, String correlationId, String replyTo) {
        if (message instanceof OutboundRequest) {
            OutboundRequest outboundRequest = (OutboundRequest) message;

            return handleMessage(outboundRequest, correlationId, replyTo);
        } else {
            VendorLogger.error("Unknow message {} " + message);
        }
        return null;
    }

    private IMessage handleMessage(OutboundRequest outboundRequest, String correlationId, String replyTo) {
        VendorLogger.debug("Inside handleMessage: getVersion");

        ServiceProxy serviceProxy = getServiceProxy(outboundRequest);

        biz.gametechclubs.casino.game.external.schemas.VersionResponse response = null;
        try {
            response = serviceProxy.version(new VersionRequest());
        } catch (RemoteException e) {
            VendorLogger.error("Exception in sending request : ", e);
        }

        GetVersionResponse getVersionResponse = new GetVersionResponse();
        if (response != null) {
            getVersionResponse.setVersion(response.getVersion());
        }

        VendorLogger.debug("Inside handleMessage: response :" + getVersionResponse);

        if (replyTo != null && !replyTo.isEmpty()) {
            VendorMessageProcessor.sendResponseToVendorMessageInvoker(replyTo, correlationId, getVersionResponse);
            return null;
        } else {
            return null;
        }
    }

}
