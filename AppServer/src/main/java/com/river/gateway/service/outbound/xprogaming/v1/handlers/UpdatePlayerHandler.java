package com.river.gateway.service.outbound.xprogaming.v1.handlers;

import java.rmi.RemoteException;

import com.river.gateway.service.beans.UserData;
import com.river.gateway.service.listener.object.UserLoginData;
import com.river.gateway.service.logger.VendorLogger;
import com.river.gateway.service.message.handlers.IMessageHandler;
import com.river.gateway.service.message.listener.VendorMessageProcessor;
import com.river.gateway.service.messages.common.IMessage;
import com.river.gateway.service.messages.outbound.OutboundUserRequest;
import com.river.gateway.service.messages.outbound.OutboundUserResponse;
import com.river.gateway.service.outbound.xprogaming.v1.XProGamingOutboundAdaptor;
import com.river.gateway.service.outbound.xprogaming.v1.messages.CreateUpdatePlayerResponse;
import com.river.gateway.service.outbound.xprogaming.v1.messages.UpdatePlayerRequest;

import biz.gametechclubs.casino.game.external.schemas.ServiceProxy;

public class UpdatePlayerHandler extends AbstractVendorHandler implements IMessageHandler {

    @Override
    public IMessage processOutboundMessage(IMessage message, String correlationId, String replyTo) {
        if (message instanceof OutboundUserRequest) {
            OutboundUserRequest outboundUserRequest = (OutboundUserRequest) message;

            return handleMessage(outboundUserRequest, correlationId, replyTo);
        } else {
            VendorLogger.error("Unknow message {} " + message);
        }
        return null;
    }

    private IMessage handleMessage(OutboundUserRequest outboundUserRequest, String correlationId, String replyTo) {

        UpdatePlayerRequest updatePlayerRequest = XProGamingOutboundAdaptor.getUpdatePlayerRequest(outboundUserRequest);
        VendorLogger.debug("Inside handleMessage: " + updatePlayerRequest);

        ServiceProxy serviceProxy = getServiceProxy(outboundUserRequest);

        biz.gametechclubs.casino.game.external.schemas.UpdatePlayerResponse response = null;
        try {
            response =
                    serviceProxy.updatePlayer(new biz.gametechclubs.casino.game.external.schemas.UpdatePlayerRequest(
                            updatePlayerRequest.getSecureLogin(), updatePlayerRequest.getSecurePassword(),
                            updatePlayerRequest.getPlayerID(), updatePlayerRequest.getNickname(), updatePlayerRequest
                                    .getEmail(), updatePlayerRequest.getLanguage(), updatePlayerRequest.getFirstName(),
                            updatePlayerRequest.getLastName(), updatePlayerRequest.getBanStatus()));
        } catch (RemoteException e) {
            VendorLogger.error("Exception in sending request : ", e);
        }

        CreateUpdatePlayerResponse createUpdatePlayerResponse = new CreateUpdatePlayerResponse();
        if (response != null) {
            createUpdatePlayerResponse.setDescription(response.getDescription());
            createUpdatePlayerResponse.setError(response.getError());
            createUpdatePlayerResponse.setPlayerID(response.getPlayerID());
        }

        VendorLogger.debug("Inside handleMessage: response :" + createUpdatePlayerResponse);

        if (createUpdatePlayerResponse != null) {

            UserLoginData userLoginData = XProGamingOutboundAdaptor.getUserLoginData(outboundUserRequest);
            // update the cache with playerId and DB
            UserData userData = new UserData();
            userData.setEcrCurrency(userLoginData.getEcrCurrency());
            userData.setEcrExternalId(userLoginData.getEcrExternalID());
            userData.setEcrId(userLoginData.getEcrID());
            userData.setEcrVendorId(String.valueOf(createUpdatePlayerResponse.getPlayerID()));
            userData.setUserLoginData(userLoginData);
            userData.setVendorId(XProGamingOutboundAdaptor.vendorId.toString());
            vendorService.updateUserData(userData);

            OutboundUserResponse outboundUserResponse =
                    XProGamingOutboundAdaptor.getOutboundCreateUpdatePlayerResponse(outboundUserRequest,
                            createUpdatePlayerResponse);

            if (replyTo != null && !replyTo.isEmpty()) {
                VendorMessageProcessor.sendResponseToVendorMessageInvoker(replyTo, correlationId, outboundUserResponse);
            } else {
                return outboundUserResponse;
            }
        }
        return null;
    }

}
