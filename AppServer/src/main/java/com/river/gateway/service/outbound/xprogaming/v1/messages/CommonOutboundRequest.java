package com.river.gateway.service.outbound.xprogaming.v1.messages;

/**
 * 
 * @author Dilip Reddy
 * 
 */
public class CommonOutboundRequest {

    /**
     * User name for authentication in the Casino Game API service.
     */
    private String secureLogin;

    /**
     * Password for authentication in the Casino Game API service.
     */
    private String securePassword;

    public String getSecureLogin() {
        return secureLogin;
    }

    public void setSecureLogin(String secureLogin) {
        this.secureLogin = secureLogin;
    }

    public String getSecurePassword() {
        return securePassword;
    }

    public void setSecurePassword(String securePassword) {
        this.securePassword = securePassword;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("VendorOutboundBaseMessage [secureLogin=");
        builder.append(secureLogin);
        builder.append(", securePassword=");
        builder.append(securePassword);
        builder.append("]");
        return builder.toString();
    }

}