package com.river.gateway.service.outbound.xprogaming.v1.messages;

public class CreateFRBResponse {

	private String error;
	private String description;
	private long providerPlayerID;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getProviderPlayerID() {
		return providerPlayerID;
	}
	public void setProviderPlayerID(long providerPlayerID) {
		this.providerPlayerID = providerPlayerID;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateFRBResponse [error=");
		builder.append(error);
		builder.append(", description=");
		builder.append(description);
		builder.append(", providerPlayerID=");
		builder.append(providerPlayerID);
		builder.append("]");
		return builder.toString();
	}
	
}
