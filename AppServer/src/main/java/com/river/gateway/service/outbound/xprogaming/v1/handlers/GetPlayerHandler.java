package com.river.gateway.service.outbound.xprogaming.v1.handlers;

import java.rmi.RemoteException;

import com.river.gateway.service.logger.VendorLogger;
import com.river.gateway.service.message.handlers.IMessageHandler;
import com.river.gateway.service.message.listener.VendorMessageProcessor;
import com.river.gateway.service.messages.common.IMessage;
import com.river.gateway.service.messages.outbound.OutboundUserRequest;
import com.river.gateway.service.messages.outbound.OutboundUserResponse;
import com.river.gateway.service.outbound.xprogaming.v1.XProGamingOutboundAdaptor;
import com.river.gateway.service.outbound.xprogaming.v1.messages.GetPlayerRequest;
import com.river.gateway.service.outbound.xprogaming.v1.messages.GetPlayerResponse;

import biz.gametechclubs.casino.game.external.schemas.ServiceProxy;

public class GetPlayerHandler extends AbstractVendorHandler implements IMessageHandler {

    @Override
    public IMessage processOutboundMessage(IMessage message, String correlationId, String replyTo) {
        if (message instanceof OutboundUserRequest) {
            OutboundUserRequest outboundUserRequest = (OutboundUserRequest) message;

            return handleMessage(outboundUserRequest, correlationId, replyTo);
        } else {
            VendorLogger.error("Unknow message {} " + message);
        }
        return null;
    }

    private IMessage handleMessage(OutboundUserRequest outboundUserRequest, String correlationId, String replyTo) {

        GetPlayerRequest getPlayerRequest = XProGamingOutboundAdaptor.getPlayerRequest(outboundUserRequest);
        VendorLogger.debug("Inside handleMessage: " + getPlayerRequest);

        ServiceProxy serviceProxy = getServiceProxy(outboundUserRequest);

        biz.gametechclubs.casino.game.external.schemas.GetPlayerResponse response = null;
        try {
            response =
                    serviceProxy.getPlayer(new biz.gametechclubs.casino.game.external.schemas.GetPlayerRequest(
                            getPlayerRequest.getSecureLogin(), getPlayerRequest.getSecurePassword(), getPlayerRequest
                                    .getPlayerID()));
        } catch (RemoteException e) {
            VendorLogger.error("Exception in sending request : ", e);
        }

        GetPlayerResponse getPlayerResponse = new GetPlayerResponse();
        if (response != null) {
            // getPlayerResponse.setBanStatus(response.getBanStatus());
            getPlayerResponse.setPlayerID(response.getPlayerID());
        }

        VendorLogger.debug("Inside handleMessage: response :" + getPlayerResponse);

        OutboundUserResponse outboundUserResponse =
                XProGamingOutboundAdaptor.getOutboundPlayerResponse(outboundUserRequest, getPlayerResponse);

        if (replyTo != null && !replyTo.isEmpty()) {
            VendorMessageProcessor.sendResponseToVendorMessageInvoker(replyTo, correlationId, outboundUserResponse);
            return null;
        } else {
            return outboundUserResponse;
        }
    }

}
