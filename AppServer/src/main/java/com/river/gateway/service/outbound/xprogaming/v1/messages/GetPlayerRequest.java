package com.river.gateway.service.outbound.xprogaming.v1.messages;

/**
 * 
 * @author Dilip Reddy
 * 
 */
public class GetPlayerRequest extends CommonOutboundRequest {

    /**
     * ID of End User on the side of Topgame system. The Operator will receive this ID in response to CreatePlayer
     * method. Example: 182226
     */
    private long playerID;

    public long getPlayerID() {
        return playerID;
    }

    public void setPlayerID(long playerID) {
        this.playerID = playerID;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GetPlayerRequest [playerID=");
        builder.append(playerID);
        builder.append("]");
        return builder.toString();
    }

}