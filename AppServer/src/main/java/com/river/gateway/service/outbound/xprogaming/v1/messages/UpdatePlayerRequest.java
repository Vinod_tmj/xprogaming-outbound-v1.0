package com.river.gateway.service.outbound.xprogaming.v1.messages;

/**
 * 
 * @author Dilip Reddy
 * 
 */
public class UpdatePlayerRequest extends CommonOutboundRequest {

    /**
     * ID of End User on the side of Topgame system. The Operator will receive this ID in response to CreatePlayer
     * method. Example: 182226
     */
    private long playerID;

    /**
     * Nickname of the End User in the Operator system.
     */
    private String nickname;

    /**
     * Email of the End User.
     */
    private String email;

    /**
     * Language of the End User. Example: en
     */
    private String language;

    /**
     * First name of the End User.
     */
    private String firstName;

    /**
     * Last name of the End User.
     */
    private String lastName;

    /**
     * End User's chat ban status. Possible values: 0 chat is available for the End User 1  End User is banned, chat
     * is not available.
     */
    private boolean banStatus;

    public long getPlayerID() {
        return playerID;
    }

    public void setPlayerID(long playerID) {
        this.playerID = playerID;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean getBanStatus() {
        return banStatus;
    }

    public void setBanStatus(boolean banStatus) {
        this.banStatus = banStatus;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UpdatePlayerRequest [playerID=");
        builder.append(playerID);
        builder.append(", nickname=");
        builder.append(nickname);
        builder.append(", email=");
        builder.append(email);
        builder.append(", language=");
        builder.append(language);
        builder.append(", firstName=");
        builder.append(firstName);
        builder.append(", lastName=");
        builder.append(lastName);
        builder.append(", banStatus=");
        builder.append(banStatus);
        builder.append("]");
        return builder.toString();
    }

}