package com.river.gateway.service.outbound.xprogaming.v1.messages;

/**
 * 
 * @author Dilip Reddy
 * 
 */
public class CreateUpdatePlayerResponse extends CommonOutboundResponse {

    /**
     * ID of the End User in Topgame system. Example: 182226 This field will be empty if an error occurred while
     * processing.
     */
    private long playerID;

    public long getPlayerID() {
        return playerID;
    }

    public void setPlayerID(long playerID) {
        this.playerID = playerID;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CreateUpdatePlayerResponse [playerID=");
        builder.append(playerID);
        builder.append(", error=");
        builder.append(getError());
        builder.append(", description=");
        builder.append(getDescription());
        builder.append("]");
        return builder.toString();
    }

}