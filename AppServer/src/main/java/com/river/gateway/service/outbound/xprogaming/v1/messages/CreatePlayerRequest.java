package com.river.gateway.service.outbound.xprogaming.v1.messages;

/**
 * 
 * @author Dilip Reddy
 * 
 */
public class CreatePlayerRequest extends CommonOutboundRequest {

    /**
     * Nickname of the End User in the Operator system.
     */
    private String nickname;

    /**
     * Email of the End User.
     */
    private String email;

    /**
     * Currency of the End User. 3 character ISO 4217 code. Example: USD
     */
    private String currency;

    /**
     * Language of the End User. Example: en
     */
    private String language;

    /**
     * First name of the End User.
     */
    private String firstName;

    /**
     * Last name of the End User.
     */
    private String lastName;

    /**
     * End User's account ID in the Operator system.
     */
    private String accountID;

    /**
     * End User's chat ban status. Possible values: 0  chat is available for the End User 1 .End User is banned, chat
     * is not available.
     */
    private boolean banStatus;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public boolean getBanStatus() {
        return banStatus;
    }

    public void setBanStatus(boolean banStatus) {
        this.banStatus = banStatus;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CreatePlayerRequest [nickname=");
        builder.append(nickname);
        builder.append(", email=");
        builder.append(email);
        builder.append(", currency=");
        builder.append(currency);
        builder.append(", language=");
        builder.append(language);
        builder.append(", firstName=");
        builder.append(firstName);
        builder.append(", lastName=");
        builder.append(lastName);
        builder.append(", accountID=");
        builder.append(accountID);
        builder.append(", banStatus=");
        builder.append(banStatus);
        builder.append("]");
        return builder.toString();
    }

}