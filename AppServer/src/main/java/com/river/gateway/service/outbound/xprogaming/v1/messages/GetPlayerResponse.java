package com.river.gateway.service.outbound.xprogaming.v1.messages;

/**
 * 
 * @author Dilip Reddy
 * 
 */
public class GetPlayerResponse {

    /**
     * ID of the End User in Topgame system. Example: 182226 This field will be empty if an error occurred while
     * processing.
     */
    private long playerID;

    /**
     * End User's chat ban status. Possible values: 0 chat is available for End User 1 End User is banned, chat is
     * not available.
     */
    private int banStatus;

    public long getPlayerID() {
        return playerID;
    }

    public void setPlayerID(long playerID) {
        this.playerID = playerID;
    }

    public int getBanStatus() {
        return banStatus;
    }

    public void setBanStatus(int banStatus) {
        this.banStatus = banStatus;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GetPlayerResponse [playerID=");
        builder.append(playerID);
        builder.append(", banStatus=");
        builder.append(banStatus);
        builder.append("]");
        return builder.toString();
    }

}